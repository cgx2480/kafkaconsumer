package com.fgi.kafka;

public class ReturnInfo {
    private String sirc_tablename;
    private String sirc_createtime;
    private String sirc_pch;
    private int rowCount;
    private int lastRowNumber;
    private String oracle;
    private String pushfinishtime;

    ReturnInfo() {
    }

    private String getPushfinishtime() {
        return pushfinishtime;
    }

    void setPushfinishtime(String pushfinishtime) {
        this.pushfinishtime = pushfinishtime;
    }

    private String getSirc_tablename() {
        return sirc_tablename;
    }

    void setSirc_tablename(String sirc_tablename) {
        this.sirc_tablename = sirc_tablename;
    }

    private String getSirc_createtime() {
        return sirc_createtime;
    }

    void setSirc_createtime(String sirc_createtime) {
        this.sirc_createtime = sirc_createtime;
    }

    private String getSirc_pch() {
        return sirc_pch;
    }

    void setSirc_pch(String sirc_pch) {
        this.sirc_pch = sirc_pch;
    }

    private int getRowCount() {
        return rowCount;
    }

    void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    private int getLastRowNumber() {
        return lastRowNumber;
    }

    void setLastRowNumber(int lastRowNumber) {
        this.lastRowNumber = lastRowNumber;
    }

    private String getOracle() {
        return oracle;
    }

    void setOracle(String oracle) {
        this.oracle = oracle;
    }

    @Override
    public String toString() {
        return
                " " + getSirc_tablename() + "_" + getSirc_createtime() + "_" + getLastRowNumber() + "_" + getSirc_pch() +
                        " 行数:" + getRowCount() +
                        " oracle更新:" + getOracle() +
                        " 推送时间:" + getPushfinishtime();
    }
}
