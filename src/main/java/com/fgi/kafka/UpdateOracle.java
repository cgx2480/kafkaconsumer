package com.fgi.kafka;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimerTask;

public class UpdateOracle extends TimerTask {

    @Override
    public void run() {
        try {

            Connection jdbcConnection = Appmain.connectionPool.getConnection();
            Statement statement = jdbcConnection.createStatement();
            if (Appmain.successSqls.size() > 0) {
                for (String successSql : Appmain.successSqls) {
                    statement.addBatch(successSql);
                }

                try {
                    int[] h = statement.executeBatch();
                    int count = 0;
                    for (int i : h) {
                        if (i >= 1) {
                            count += 1;
                        }
                    }
                    if (count > 0) {
                        System.out.println("更新oracle : 待处理/失败已重推->处理中 : " + count + "\n");
                    }
                } catch (SQLException e) {
                    jdbcConnection.rollback();
                    System.out.println("SQLException : " + e.getMessage());
                } finally {
//                    statement.clearBatch();
                    statement.close();
                    jdbcConnection.close();
                }
            }
            Appmain.successSqls.clear();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.gc();
        }
    }

}
