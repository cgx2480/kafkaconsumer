package com.fgi.kafka;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.map.HashedMap;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static com.fgi.kafka.Appmain.*;

class HTableWriter {
    private static final String[] columns = {
            "sirc_tablename",
            "sirc_createtime",
            "sirc_mode",
            "sirc_status",
            "sirc_explain",
            "sirc_classcode",
            "sirc_orgcode",
            "sirc_orgname",
            "sirc_orgid",
            "sirc_mattername",
            "sirc_matterid",
            "sirc_datatype",
            "sirc_mainkey",
            "sirc_project",
            "sirc_validatestatus",
            "sirc_pch",
            "sirc_dataformat",
            "sirc_datanum",
    };
    private static final String updateSql = "update insertbatch set state=?,startoperatetime=to_date(?,'YYYY/MM/DD HH24:MI:SS') where state in ('待处理','失败已重推','失败') and createtimelong=? and pch=?";
    private static final String searchSql = "select * from insertbatch where createtimelong=? and pch=?";

    private Set<String> sirc_mainkeys;
    private StringBuilder rowkeyPrefix;
    private Map<String, String> messageMap;
    private Set<String> setTemp;
    private StringBuilder compareStr;


    HTableWriter() throws ClassNotFoundException {
        Class.forName(DBDRIVER);
        rowkeyPrefix = new StringBuilder();
        setTemp = new HashSet<>();
        compareStr = new StringBuilder();
    }

    ReturnInfo writeHtable(String message, String topicName) throws SQLException {
//        System.out.println(message);
        ReturnInfo returnInfo = new ReturnInfo();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            //判断消息是本体还是rowkey
            if (message.startsWith("{")) {
                messageMap = objectMapper.readValue(message, Map.class);
            } else {
                Get get = new Get(Bytes.toBytes(message));
                Result result;
                result = kafkalog.get(get);
                assert result != null;
                String message2 = new String(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("data")));
                messageMap = objectMapper.readValue(message2, Map.class);
            }

            for (String column : columns) {
                if (!messageMap.keySet().contains(column)) {
                    messageMap.put(column, "");
                }
            }

            messageMap.put("sirc_status", "处理中");

            String sirc_createtime = messageMap.get("sirc_createtime");
            String sirc_dataformat = messageMap.get("sirc_dataformat");
            String sirc_mainkey = messageMap.get("sirc_mainkey");
            String sirc_pch = messageMap.get("sirc_pch");
            String sirc_tablename = messageMap.get("sirc_tablename");


            sirc_mainkeys = new HashSet<>();
            if (messageMap.get("sirc_mode").equals("excel上传") && sirc_mainkey != null && !sirc_mainkey.equals("")) {
                sirc_mainkeys.addAll(Arrays.asList(sirc_mainkey.split(",")));
            }

            String sirc_data = new String(messageMap.get("sirc_data").getBytes());
            String sirc_data_decode = new String(Base64.decodeBase64(sirc_data));

            //rowkey前半段
            rowkeyPrefix.replace(0, rowkeyPrefix.length(), sirc_tablename + "_" + sirc_createtime);

            //重推消息将计数写为1
            if (topicName.equals(flags.getTopicagain())) {
                setRowkeySuffix(rowkeyPrefix.toString(), 1);
                log.error(message);
            }

            //rowkey计数
            int rowkeySuffix = Integer.parseInt(getRowkeySuffix(rowkeyPrefix.toString()));

            // 解析并写入
            Map<String, Integer> map = new HashMap<>();
            if (!sirc_data.equals("")) {
                if (sirc_dataformat.equals("json")) {
                    map = writeJsonMessage(sirc_data_decode, rowkeySuffix);
                } else if (sirc_dataformat.equals("xml")) {
                    map = writeXmlMessage(sirc_data_decode, rowkeySuffix);
                }
            }


            // 清理变量
            messageMap.clear();
            sirc_mainkeys.clear();
            setTemp.clear();
            compareStr.delete(0, compareStr.length());

            // 终端输出内容
            int lastRowCount = Integer.parseInt(getRowkeySuffix(rowkeyPrefix.toString())) - 1;
            returnInfo.setSirc_tablename(sirc_tablename);
            returnInfo.setSirc_createtime(sirc_createtime);
            returnInfo.setSirc_pch(sirc_pch);
            returnInfo.setRowCount(map.get("rowCount"));//行数
            returnInfo.setLastRowNumber(lastRowCount);

            Map<String, String> updateOracleMap = null;
            if (map.get("success") == 1) {
                updateOracleMap = updateOracle(sirc_createtime, sirc_pch, true);
                String updateOracleResult = updateOracleMap.get("oracle");
                returnInfo.setOracle(updateOracleResult);
                if (updateOracleResult.equals("0")) {
                    String sql = "update insertbatch set state='处理中' where state in ('待处理','失败已重推','失败') and createtimelong='" + sirc_createtime + "' and pch='" + sirc_pch + "'";
                    Appmain.successSqls.add(sql);
                }
            } else if (map.get("success") == 0) {
//                returnInfo.setOracle(updateOracle(sirc_createtime, sirc_pch, false).get("oracle"));
                updateOracleMap = updateOracle(sirc_createtime, sirc_pch, false);
                returnInfo.setOracle(updateOracleMap.get("oracle"));
            }

            assert updateOracleMap != null;
            returnInfo.setPushfinishtime(updateOracleMap.get("pushfinishtime"));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.gc();
        }
        return returnInfo;
    }

    private Map<String, String> updateOracle(String sirc_createtime, String sirc_pch, boolean success) throws SQLException {
        Map<String, String> map = new HashedMap();
        Calendar calendar = new GregorianCalendar();
        String date = calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + " ";
        String time = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
        Connection jdbcConnection = connectionPool.getConnection();

        PreparedStatement statement = jdbcConnection.prepareStatement(updateSql);
        if (success) {
            statement.setString(1, "处理中");
        } else {
            statement.setString(1, "失败");
        }
        statement.setString(2, date + time);
        statement.setString(3, sirc_createtime);
        statement.setString(4, sirc_pch);
        int i = statement.executeUpdate();

        PreparedStatement statement2 = jdbcConnection.prepareStatement(searchSql);
        statement2.setString(1, sirc_createtime);
        statement2.setString(2, sirc_pch);
        ResultSet resultSet = statement2.executeQuery();

        while (resultSet.next()) {
            map.put("pushfinishtime", resultSet.getString("pushfinishtime"));
        }

        statement.close();
        jdbcConnection.close();
        map.put("oracle", Integer.toString(i));

        return map;
    }

    /**
     * xml类型解析
     */
    private Map<String, Integer> writeXmlMessage(String sirc_data_decode, int rowkeySuffix) {
        messageMap.remove("sirc_data");
//        System.out.println(sirc_data_decode);
        int rowCount = 0;
        boolean flag = true;
        Map<String, Integer> result = new HashMap<>();
        try {
            List<Put> puts = new ArrayList<>();
            List<Put> putsForIsattachment = new ArrayList<>();
            Document messageDocument = DocumentHelper.parseText(sirc_data_decode);

            //<table>
            Element table = messageDocument.getRootElement();
            List<Element> rows = table.elements();

            //一行
            for (Element row : rows) {

                //拼凑行键rowkey
                String rowkey = rowkeyPrefix + "_" + rowkeySuffix + "_" + messageMap.get("sirc_pch");
                Put put = new Put(Bytes.toBytes(rowkey));

                String typeOfRow = row.attributeValue("type");

                // 根据type属性设置sirc_flag的值
                if (typeOfRow.equals("add") || typeOfRow.equals("update")) {
                    messageMap.put("sirc_flag", "0");
                } else if (typeOfRow.equals("delete")) {
                    messageMap.put("sirc_flag", "1");
                }

                for (Element cell : row.elements()) {
                    if (sirc_mainkeys.size() > 0) {
                        if (sirc_mainkeys.contains(cell.getName())) {
                            compareStr.append(cell.getTextTrim()).append(" ");
                        }
                    }

                    // 属性isattachment不为空并且值等于true
//                    String isattachment = cell.attributeValue("isattachment");
//                    if (isattachment != null && isattachment.equals("true")) {
//                        byte[] val = Base64.decodeBase64(cell.getTextTrim());
//                        String md5 = getMD5(val);
//                        Put put1 = new Put(Bytes.toBytes(messageMap.get("sirc_tablename") + "_" + md5));
//                        put1.addColumn(Bytes.toBytes("info"), Bytes.toBytes("data"), val);
//                        putsForIsattachment.add(put1);
//                        put.addColumn(Bytes.toBytes("info"), Bytes.toBytes(cell.getName()),
//                                Bytes.toBytes(messageMap.get("sirc_tablename") + "_" + md5));
//                    } else {
//                        put.addColumn(Bytes.toBytes("info"),
//                                Bytes.toBytes(cell.getName()),
//                                cell.getTextTrim().getBytes());
//                    }
                    put.addColumn(Bytes.toBytes("info"),
                            Bytes.toBytes(cell.getName()),
                            cell.getTextTrim().getBytes());
                }

//                if (messageMap.get("sirc_mode").equals("excel上传") && sirc_mainkeys.size() > 0) {
//                    if (!setTemp.contains(compareStr.toString())) { // 比对字段的值第一次出现
//                        messageMap.put("sirc_validatestatus", "");
//                        setTemp.add(compareStr.toString());
//                    } else { // 比对字段的值不是第一次出现
//                        messageMap.put("sirc_validatestatus", "无效");
//                        messageMap.put("sirc_validatestatus_problem", "excel中重复主键数据");
//                    }
//                } else {
//                    messageMap.put("sirc_validatestatus", "");
//                }

                setSircValidatestatus();

                for (Map.Entry<String, String> entry : messageMap.entrySet()) {
                    put.addColumn(
                            Bytes.toBytes("info"),
                            Bytes.toBytes(entry.getKey().toLowerCase()),
                            Bytes.toBytes(entry.getValue())
                    );
                }

                puts.add(put);
                rowkeySuffix += 1;
            }

            insertTable.put(puts);
            hbase_file.put(putsForIsattachment);
            setRowkeySuffix(rowkeyPrefix.toString(), rowkeySuffix);
            rowCount = rows.size();
        } catch (DocumentException e) {
            flag = false;
            log.error("格式转换错误 " + messageMap.get("sirc_pch") + "_" + messageMap.get("sirc_createtime") + sirc_data_decode + "\n", e);
        } catch (IOException e) {
            flag = false;
            log.error("hbase读写错误 " + messageMap.get("sirc_pch") + "_" + messageMap.get("sirc_createtime") + sirc_data_decode + "\n", e);
        } finally {
            if (flag) {
                result.put("success", 1);
                result.put("rowCount", rowCount);
            } else {
//                System.out.println("处理失败");
//                String sql = String.format("update insertbatch set state='失败' where pch='%s' and createtimelong='%s'", messageMap.get("sirc_pch"), messageMap.get("sirc_createtime"));
//                faileSqls.add(sql);
                result.put("success", 0);
                result.put("rowCount", rowCount);
            }
        }
        return result;
    }

    /**
     * json类型解析
     */
    private Map<String, Integer> writeJsonMessage(String sirc_data_decode, int rowkeySuffix) {
        messageMap.remove("sirc_data");
//        System.out.println(sirc_data_decode);
        int rowCount = 0;
        boolean flag = true;
        Map<String, Integer> result = new HashMap<>();
        try {
            List<Put> puts = new ArrayList<>();
            ObjectMapper jsonMapper = new ObjectMapper();
            List<Map<String, String>> rows = jsonMapper.readValue(sirc_data_decode, List.class);

            //rows是解码后转换成的map列表，多行,cell是其中一行
            for (Map<String, String> row : rows) {
                //finalToTable存放最终解析完毕的每行数据
//                Map<String, String> finalToTable = new HashMap<>();
                //拼凑行键rowkey
                String rowkey = rowkeyPrefix + "_" + rowkeySuffix + "_" + messageMap.get("sirc_pch");
                Put put = new Put(Bytes.toBytes(rowkey));

                for (Map.Entry<String, String> cell : row.entrySet()) {
                    //如果有多个字段需要比对，将其value用分隔符连接起来
                    //每行数据都会在满足条件时生成compareStr，用来与第一次出现的compareStr对比
                    if (sirc_mainkeys.size() > 0) {
                        if (sirc_mainkeys.contains(cell.getKey())) {
                            compareStr.append(cell.getValue()).append(" ");
                        }
                    }

                    //sirc_data_map是其中一行
                    //写入每行的特有字段
                    if (!cell.getKey().equals("sirc_pch")) {
                        if (messageMap.get("sirc_tablename").equals("table36041") ||
                                !cell.getKey().equals("content")) {
                            put.addColumn(
                                    Bytes.toBytes("info"),
                                    Bytes.toBytes(cell.getKey().toLowerCase()),
                                    Bytes.toBytes(cell.getValue())
                            );
                        } else {
                            put.addColumn(
                                    Bytes.toBytes("info"),
                                    Bytes.toBytes(cell.getKey().toLowerCase()),
                                    Bytes.toBytes(cell.getValue())
                            );
                        }
                    }
                }

//                if (messageMap.get("sirc_mode").equals("excel上传") && sirc_mainkeys.size() > 0) {
//                    if (!setTemp.contains(compareStr.toString())) {
//                        //setTemp为空或者不包含对比字段的value时将其保存
//                        //并将sirc_validatestatus设为“”
//                        messageMap.put("sirc_validatestatus", "");
//                        setTemp.add(compareStr.toString());
//                    } else {
//                        //setTemp中发现compairStr已经被保存过一次
//                        //则代表重复，填入需求的字段和值
//                        messageMap.put("sirc_validatestatus", "无效");
//                        messageMap.put("sirc_validatestatus_problem", "excel中重复主键数据");
//                    }
//                } else {
//                    messageMap.put("sirc_validatestatus", "");
//                }

                setSircValidatestatus();

                for (Map.Entry<String, String> entry : messageMap.entrySet()) {
                    put.addColumn(
                            Bytes.toBytes("info"),
                            Bytes.toBytes(entry.getKey().toLowerCase()),
                            Bytes.toBytes(entry.getValue())
                    );
                }

                puts.add(put);
                rowkeySuffix += 1;
            }

            insertTable.put(puts);
            setRowkeySuffix(rowkeyPrefix.toString(), rowkeySuffix);
            rowCount = rows.size();
        } catch (JsonParseException | JsonMappingException e) {
            flag = false;
            log.error("格式转换错误 " + messageMap.get("sirc_pch") + "_" + messageMap.get("sirc_createtime") + sirc_data_decode + "\n", e);
        } catch (IOException e) {
            flag = false;
            log.error("hbase读写错误 " + messageMap.get("sirc_pch") + "_" + messageMap.get("sirc_createtime") + sirc_data_decode + "\n", e);
        } finally {
            if (flag) {
                result.put("success", 1);
                result.put("rowCount", rowCount);
            } else {
//                System.out.println("处理失败");
//                String sql = String.format("update insertbatch set state='失败' where pch='%s' and createtimelong='%s'", messageMap.get("sirc_pch"), messageMap.get("sirc_createtime"));
//                faileSqls.add(sql);
                result.put("success", 0);
                result.put("rowCount", rowCount);
            }
        }
        return result;
    }

    /**
     * 根据 sirc_mainkeys 判断 sirc_validatestatus 写入何种数据
     */
    private void setSircValidatestatus() {
        if (messageMap.get("sirc_mode").equals("excel上传") && sirc_mainkeys.size() > 0) {
            if (!setTemp.contains(compareStr.toString())) {
                //setTemp为空或者不包含对比字段的value时将其保存
                //并将sirc_validatestatus设为“”
                messageMap.put("sirc_validatestatus", "");
                setTemp.add(compareStr.toString());
            } else {
                //setTemp中发现compairStr已经被保存过一次
                //则代表重复，填入需求的字段和值
                messageMap.put("sirc_validatestatus", "无效");
                messageMap.put("sirc_validatestatus_problem", "excel中重复主键数据");
            }
        } else {
            messageMap.put("sirc_validatestatus", "");
        }
    }

    /**
     * 获取计数
     */
    private String getRowkeySuffix(String rowkeyPrefix) throws IOException {
        String suffix = null;
        if (admin.tableExists(TableName.valueOf("keytable"))) {
            Get get = new Get(Bytes.toBytes(rowkeyPrefix));
            Result result = keytable.get(get);
            if (!result.toString().equals("keyvalues=NONE")) {
                //存在前缀，返回当前计数
                suffix = new String(keytable.get(get).getValue(Bytes.toBytes("info"), Bytes.toBytes("count")));
            } else {
                Put put = new Put(Bytes.toBytes(rowkeyPrefix));
                put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("count"), Bytes.toBytes("1"));
                keytable.put(put);
                suffix = "1";
            }
        } else {
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("keytable"));
            HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(Bytes.toBytes("info"));
            hTableDescriptor.addFamily(hColumnDescriptor);
            admin.createTable(hTableDescriptor);
            getRowkeySuffix(rowkeyPrefix);
        }
        return suffix;
    }

    /**
     * 写入计数值
     */
    private void setRowkeySuffix(String rowkeyPrefix, int keySuffixNew) throws IOException {
        Put put = new Put(Bytes.toBytes(rowkeyPrefix));
        put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("count"), Bytes.toBytes(Integer.toString(keySuffixNew)));
        keytable.put(put);
    }
}
