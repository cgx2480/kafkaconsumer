package com.fgi.kafka;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据库连接池
 **/
class ConnectionPool {
    //dataSource资源只能初始化一次  ；c3p0-config.xml要放在根目录（src）底下
    //	private static DataSource dataSource = new ComboPooledDataSource("connection pool");

    private static ComboPooledDataSource source = new ComboPooledDataSource("connection pool");


    ConnectionPool(String dri, String url, String user, String passwd) {
        try {
            source.setDriverClass(dri);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        source.setJdbcUrl(url);
        source.setUser(user);
        source.setPassword(passwd);
    }

    /**
     * 获取连接
     */
    Connection getConnection() throws SQLException {
        Connection conn;
        conn = source.getConnection();
        return conn;
    }

}
