package com.fgi.kafka;

import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ConsumerThread implements Runnable {
    private HTableWriter writer;
    private KafkaStream m_stream;
    private int m_threadNumber;
    private String topicName;
    private Calendar calendar;

    ConsumerThread(KafkaStream a_stream, int a_threadNumber, String topic) throws ClassNotFoundException {
        m_threadNumber = a_threadNumber;
        m_stream = a_stream;
        topicName = topic;
        writer = new HTableWriter();

        calendar = new GregorianCalendar();
//        String date = calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + " ";
//        String time = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);


        System.out.printf("线程 %d 已就绪%n", a_threadNumber);
    }

    public void run() {
        for (MessageAndMetadata<String, String> mam : ((Iterable<MessageAndMetadata<String, String>>) m_stream)) {

            ReturnInfo returnInfo = null;
            try {
                returnInfo = writer.writeHtable(mam.message(), topicName);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            assert returnInfo != null;
//            System.out.printf("[%s] in thread [%s]%s%n", topicName, Thread.currentThread().getName(), returnInfo.toString());
            System.out.println("[" + topicName + "] [" + Thread.currentThread().getName() + "]" + returnInfo.toString());

            Appmain.consumer.commitOffsets();
        }
        System.out.println("Shutting down Thread: " + m_threadNumber);
    }

//    public void run() {
//        for (MessageAndMetadata<String, String> mam : ((Iterable<MessageAndMetadata<String, String>>) m_stream)) {
//
//            if (!mam.topic().equals(Appmain.flags.getTopicagain())) {
//                ReturnInfo returnInfo = null;
//                try {
//                    returnInfo = writer.writeHtable(mam.message(), topicName);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//                assert returnInfo != null;
////            System.out.printf("[%s] in thread [%s]%s%n", topicName, Thread.currentThread().getName(), returnInfo.toString());
//                System.out.println("[" + topicName + "] [" + Thread.currentThread().getName() + "]" + returnInfo.toString());
//            } else {
//                System.out.println("忽略重推 " + calendar.getTime());
//            }
//
//            Appmain.consumer.commitOffsets();
//        }
//        System.out.println("Shutting down Thread: " + m_threadNumber);
//    }
}
