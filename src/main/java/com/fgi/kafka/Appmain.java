package com.fgi.kafka;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.serializer.StringDecoder;
import kafka.utils.VerifiableProperties;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Appmain {
    final static String DBDRIVER = "oracle.jdbc.driver.OracleDriver";
    static final String TOPIC = "topic";
    static final String TOPICAGAIN = "topicagain";
    static final String GROUPID = "groupid";
    static final String THREAD = "thread";
    static final String HOSTNAMES = "hostnames";
    static final String ZKNODE = "zknode";
    static final String ORACLEURL = "oracleurl";
    static final String ORACLEUSERNAME = "oracleusername";
    static final String ORACLEPASSWORD = "oraclepassword";
    static final String ORACLEUPDATETIME = "oracleupdatetime";
    private static final Options THE_OPTIONS = createOptions();
    static ConsumerConnector consumer;
    //连接oracle
    static ConnectionPool connectionPool;
    static Flags flags;
    static List<String> successSqls;
    static Logger log = Logger.getLogger("Kafka_Error");
    static Table insertTable;
    static Table keytable;
    static Table hbase_file;
    static Table kafkalog;
    static Admin admin;
    private final String topic;

    private Appmain(String a_zookeeper, String a_groupId, String a_topic) throws IOException {
        consumer = kafka.consumer.Consumer.createJavaConsumerConnector(
                createConsumerConfig(a_zookeeper, a_groupId));
        this.topic = a_topic;
        successSqls = new ArrayList<>();

        //hbase
        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", flags.getHostnames());
        configuration.set("dfs.socket.timeout", "180000");
        configuration.set("hbase.client.write.buffer", "20971520");
        // hbase连接
        Connection connection = ConnectionFactory.createConnection(configuration);
        //连接hbase
        admin = connection.getAdmin();

        insertTable = connection.getTable(TableName.valueOf("inserttable"));
        if (admin.isTableAvailable(TableName.valueOf("inserttable"))) {
            System.out.println("已连接表 inserttable");
        }

        keytable = connection.getTable(TableName.valueOf("keytable"));
        if (admin.isTableAvailable(TableName.valueOf("keytable"))) {
            System.out.println("已连接表 keytable");
        }

        hbase_file = connection.getTable(TableName.valueOf("hbase_file"));
        if (admin.isTableAvailable(TableName.valueOf("hbase_file"))) {
            System.out.println("已连接表 hbase_file");
        }

        kafkalog = connection.getTable(TableName.valueOf("kafkalog"));
        if (admin.isTableAvailable(TableName.valueOf("kafkalog"))) {
            System.out.println("已连接表 kafkalog");
        }
    }

    private static Options createOptions() {
        Options options = new Options();
        options.addOption(new Option(TOPIC, true, "topic name of kafka"));
        options.addOption(new Option(TOPICAGAIN, true, "topic name of kafka"));
        options.addOption(new Option(GROUPID, true, "groupid"));
        options.addOption(new Option(THREAD, true, "thread number"));
        options.addOption(new Option(HOSTNAMES, true, "hostnames of zookeeper"));
        options.addOption(new Option(ZKNODE, true, "zookeeper.chroot"));
        options.addOption(new Option(ORACLEURL, true, "Connect oracle database address"));
        options.addOption(new Option(ORACLEUSERNAME, true, "oracle database user name"));
        options.addOption(new Option(ORACLEPASSWORD, true, "oracle database user password"));
        options.addOption(new Option(ORACLEUPDATETIME, true, "Update the oracle database time interval"));
        return options;
    }

    private static ConsumerConfig createConsumerConfig(String a_zookeeper, String a_groupId) {
        Properties props = new Properties();
        props.put("zookeeper.connect", a_zookeeper);
        props.put("group.id", a_groupId);
        props.put("fetch.message.max.bytes", "10485760");
        props.put("zookeeper.session.timeout.UpdateOracle", "400");
        props.put("zookeeper.sync.time.ms", "200");
        props.put("auto.commit.enable", "false");
//        props.put("auto.offset.reset", "largest");
        return new ConsumerConfig(props);
    }

    public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
        Flags.setFromCommandLineArgs(THE_OPTIONS, args);
        flags = Flags.getTheInstance();
        System.out.println(flags.toString());

        connectionPool = new ConnectionPool(
                DBDRIVER,
                flags.getOracleUrl(),
                flags.getOracleUserName(),
                flags.getOraclePassword()
        );

//        Timer timer3 = new Timer();
//        timer3.schedule(new UpdateOracle(), 1000, Integer.parseInt(flags.getOracleUpdateTime()) * 1000);
//        System.out.println("定时器3已设置 " + timer3.toString());

//        Timer timer4 = new Timer();
//        timer4.schedule(new CheckDataExist(), 1500, 1000 * 60 * 60);
//        System.out.println("定时器4已设置 " + timer4.toString());

        String zooKeeper = String.format("%s", flags.getHostnames());
        System.out.println(zooKeeper);
//        String groupId = "test-consumer2";
//        String groupId = "kouan-testgroup";
        String groupId = flags.getGroupid();
        String topic = flags.getTopic();
        String topicAgain = flags.getTopicagain();
        int threads = Integer.parseInt(flags.getThread());

        Appmain consumer1 = new Appmain(zooKeeper, groupId, topic);
        consumer1.runConsumer(threads);

        Appmain consumer2 = new Appmain(zooKeeper, groupId, topicAgain);
        consumer2.runConsumer(threads);

        Thread.sleep(0);
    }

    private void runConsumer(int a_numThreads) throws ClassNotFoundException {
        Map<String, Integer> topicCountMap = new HashMap<>();
        topicCountMap.put(topic, a_numThreads);

        StringDecoder keyDecoder = new StringDecoder(new VerifiableProperties());
        StringDecoder valueDecoder = new StringDecoder(new VerifiableProperties());

        Map<String, List<KafkaStream<String, String>>> consumerMap = consumer.createMessageStreams(
                topicCountMap,
                keyDecoder,
                valueDecoder
        );
        List<KafkaStream<String, String>> streams = consumerMap.get(topic);
        ExecutorService executor = Executors.newFixedThreadPool(a_numThreads);
        System.out.println(topic + " start...");
        int threadNumber = 0;

        for (final KafkaStream stream : streams) {
            executor.submit(new ConsumerThread(stream, threadNumber, topic));
            threadNumber++;
        }
    }
}
