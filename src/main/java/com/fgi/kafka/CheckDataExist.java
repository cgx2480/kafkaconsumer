package com.fgi.kafka;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimerTask;

public class CheckDataExist extends TimerTask {
    @Override
    public void run() {

        try {

            String updateSql = "update insertbatch set state='处理中' where pushfinishtime <= sysdate-1/144 and state in ('待处理','失败已重推')";
            System.out.println();
            System.out.println(updateSql);
            System.out.println();

            Connection jdbcConnection = Appmain.connectionPool.getConnection();
            Statement statement = jdbcConnection.createStatement();

            int i = statement.executeUpdate(updateSql);
            System.out.println("\n待处理/失败已重推(full) -> " + i);

            statement.close();
            jdbcConnection.close();
        } catch (SQLException e) {
            Appmain.log.error(e.getMessage());
        } finally {
            System.gc();
        }
    }
}
