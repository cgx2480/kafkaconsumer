package com.fgi.kafka;

import org.apache.commons.cli.*;

/**
 * 命令行解析的参数存放在此类中
 */
public class Flags {
    private static Flags THE_INSTANCE = new Flags();

    private String topic;
    private String topicagain;
    private String groupid;
    private String thread;
    private String hostnames;
    private String zknode;
    private String oracleUrl;
    private String oracleUserName;
    private String oraclePassword;
    private String oracleUpdateTime;

    private boolean initialized = false;

    static void setFromCommandLineArgs(Options options, String[] args) {
        CommandLineParser parser = new DefaultParser();

        try {
            CommandLine cli = parser.parse(options, args);
            THE_INSTANCE.topic = cli.getOptionValue(Appmain.TOPIC, "test_inserttable");
            THE_INSTANCE.topicagain = cli.getOptionValue(Appmain.TOPICAGAIN, "inserttable_again");
            THE_INSTANCE.groupid = cli.getOptionValue(Appmain.GROUPID, "test-consumer2");
            THE_INSTANCE.thread = cli.getOptionValue(Appmain.THREAD);
            THE_INSTANCE.hostnames = cli.getOptionValue(Appmain.HOSTNAMES);
            THE_INSTANCE.zknode = cli.getOptionValue(Appmain.ZKNODE, "");
            THE_INSTANCE.oracleUrl = cli.getOptionValue(Appmain.ORACLEURL);
            THE_INSTANCE.oracleUserName = cli.getOptionValue(Appmain.ORACLEUSERNAME);
            THE_INSTANCE.oraclePassword = cli.getOptionValue(Appmain.ORACLEPASSWORD);
            THE_INSTANCE.oracleUpdateTime = cli.getOptionValue(Appmain.ORACLEUPDATETIME, "60");
        } catch (ParseException e) {
            THE_INSTANCE.initialized = false;
            System.err.println("Parsing failed.  Reason: \n" + e.getMessage());
        }
    }

//    public static Flags getInstance() {
//        if (THE_INSTANCE.initialized) {
//            throw new RuntimeException("Flags have not been initalized");
//        }
//
//        return THE_INSTANCE;
//    }

    static Flags getTheInstance() {
        if (THE_INSTANCE.initialized) {
            throw new RuntimeException("Flags have not been initalized");
        }
        return THE_INSTANCE;
    }

    @Override
    public String toString() {
        return "*******************************************************************" + "\n" +
                "topic name -> " + this.getTopic() + "\n" +
                "topicagain name -> " + this.getTopicagain() + "\n" +
                "group id -> " + this.getGroupid() + "\n" +
                "thread number -> " + this.getThread() + "\n" +
                "hostnames -> " + this.getHostnames() + "\n" +
                "zookeeper.chroot -> " + this.getZkNode() + "\n" +
                "oracle Url -> " + this.getOracleUrl() + "\n" +
                "oracle user name -> " + this.getOracleUserName() + "\n" +
                "oracle user password -> " + this.getOraclePassword() + "\n" +
                "oracle update time -> " + this.getOracleUpdateTime() + "\n" +
                "*******************************************************************" + "\n";
    }

    public String getGroupid() {
        return groupid;
    }

    String getZkNode() {
        return zknode;
    }

    String getThread() {
        return thread;
    }

    String getTopic() {
        return topic;
    }

    String getTopicagain() {
        return topicagain;
    }

    String getHostnames() {
        return hostnames;
    }

    String getOracleUrl() {
        return oracleUrl;
    }

    String getOracleUserName() {
        return oracleUserName;
    }

    String getOraclePassword() {
        return oraclePassword;
    }

    String getOracleUpdateTime() {
        return oracleUpdateTime;
    }
}
